
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Classe cliente, conexão com o servidor fica em seu controller.
 *  
 * Requisito: java 8
 *  
 * @author douglas
 * @authro ismaquias
 */
public class Cliente extends javafx.application.Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		System.setProperty( "javafx.userAgentStylesheetUrl", "MODENA" );
		Parent parent = FXMLLoader.load(getClass().getResource("view.fxml"));
		Scene scene = new Scene(parent);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.setTitle("Jogo da Velha");
		stage.show();
	}
}