import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


@SuppressWarnings("serial")
public class Jogador extends UnicastRemoteObject implements JogadorInterface {
	private char nome;
	private Controller controller;
		
	public Jogador(Controller controller) throws RemoteException {
		this.controller = controller;
	}

	@Override
	public char getNome() throws RemoteException {
		return nome;
	}

	@Override
	public void setNome(char nome) throws RemoteException {
		this.nome = nome;
		controller.setPlayer(nome);
	}

	@Override
	public void atualizarStatus(String status) throws RemoteException {
		controller.setStatus(status);
	}

	@Override
	public void atualizarGUI(int linha, int coluna, char valor)
			throws RemoteException {
		controller.setButton(linha, coluna, valor);
	}

	@Override
	public void vencer(int linha, int coluna, char valor)
			throws RemoteException {
		controller.setCor(linha, coluna, valor);
	}
}