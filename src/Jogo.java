import java.rmi.RemoteException;

public class Jogo implements JogoInterface {
	public static final int LOGAR = 0;
	public static final int JOGAR = 1;
	public static final int HORIZONTAL = 2;
	public static final int VERTICAL = 3;
	public static final int DIAGONAL_L = 4;
	public static final int DIAGONAL_R = 5;
	public static final char SEM_JOGADOR = '0';

	private JogadorInterface player1;
	private JogadorInterface player2;

	private JogadorInterface rodada;
	private char[][] matriz;
	private boolean jogoFinalizado;

	/**
	 * Construtor da classe Jogo.
	 * 
	 * @throws RemoteException
	 */
	public Jogo() throws RemoteException {
		iniciar();
	}
	
	@Override
	public void iniciar() throws RemoteException {
		player1 = null;
		player2 = null;
		
		matriz = new char[3][];
		jogoFinalizado = false;

		for (int i = 0; i < 3; i++) {
			matriz[i] = new char[3];
			for (int o = 0; o < 3; o++) {
				matriz[i][o] = SEM_JOGADOR;
			}
		}
	}

	/**
	 * Método chamado por cada cliente para logar no jogo. Jogo permite apenas
	 * dois jogadores.
	 * 
	 * @param jogador
	 * @return boolean
	 * @throws RemoteExcepetion
	 */
	@Override
	public boolean logar(JogadorInterface jogador) throws RemoteException {
		if (player1 == null) {
			jogador.setNome('X');
			player1 = jogador;
			player1.atualizarStatus("Aguardando jogador O...");
			System.out.println("Conectou jogador X!");
			return true;
		} else if (player2 == null) {
			player2 = jogador;
			jogador.setNome('O');
			rodada = player1;
			player1.atualizarStatus("Vez do jogador X...");
			player2.atualizarStatus("Vez do jogador X...");
			System.out.println("Conectou jogador O!");
			return true;
		} else {
			jogador.atualizarStatus("Sessão está cheia! Espere a partida terminar!");
			System.out.println("Sessão está cheia! Espere a partida terminar!");
			return false;
		}
	}

	/**
	 * Método chamado quando um cliente realizar uma jogada. É necessario
	 * informar a casa que o jogador escolheu e o jogador.
	 * 
	 * @param linha
	 * @param coluna
	 * @param jogador
	 * @return boolean
	 * @throws RemoteException
	 */
	@Override
	public boolean jogar(int linha, int coluna, JogadorInterface jogador)
			throws RemoteException {
		if (rodada.getNome() == jogador.getNome() && !jogoFinalizado) {
			matriz[linha][coluna] = jogador.getNome();
			player1.atualizarGUI(linha, coluna, jogador.getNome());
			player2.atualizarGUI(linha, coluna, jogador.getNome());

			if (rodada.getNome() == player1.getNome()) {
				rodada = player2;
				player1.atualizarStatus("Vez do jogador O...");
				player2.atualizarStatus("Vez do jogador O...");
			} else {
				rodada = player1;
				player1.atualizarStatus("Vez do jogador X...");
				player2.atualizarStatus("Vez do jogador X...");
			}

			if (vitoria(jogador.getNome())) {
				player1.atualizarStatus("Jogador " + jogador.getNome()
						+ " venceu!");
				player2.atualizarStatus("Jogador " + jogador.getNome()
						+ " venceu!");
				jogoFinalizado = true;
				iniciar();
			}
			return true;
		}
		return false;
	}

	/**
	 * Método para acessar o atributo player1.
	 * 
	 * @return player1
	 * @throws RemoteException
	 */
	@Override
	public JogadorInterface getPlayer1() throws RemoteException {
		return player1;
	}

	/**
	 * Método para alterar o atributo player1.
	 * 
	 * @param player1
	 * @throws RemoteException
	 */
	@Override
	public void setPlayer1(JogadorInterface player1) throws RemoteException {
		this.player1 = player1;
	}

	/**
	 * Método para acessar o atributo player2.
	 * 
	 * @throws player2
	 */
	@Override
	public JogadorInterface getPlayer2() throws RemoteException {
		return player2;
	}

	/**
	 * Método para alterar o atributo player2.
	 * 
	 * @param player2
	 * @throws RemoteException
	 */
	@Override
	public void setPlayer2(JogadorInterface player2) throws RemoteException {
		this.player2 = player2;
	}

	@Override
	public JogadorInterface getRodada() throws RemoteException {
		return rodada;
	}

	@Override
	public void setRodada(JogadorInterface rodada) throws RemoteException {
		this.rodada = rodada;
	}

	@Override
	public boolean vitoria(char valor) throws RemoteException {
		for (int i = 0; i < 3; i++) {
			if ((matriz[i][0] == matriz[i][1])
					&& (matriz[i][1] == matriz[i][2])
					&& (matriz[i][0] == valor)) {
				selecionarModo(i, 0, valor, HORIZONTAL);
				return true;
			} else if ((matriz[0][i] == matriz[1][i])
					&& (matriz[1][i] == matriz[2][i])
					&& (matriz[0][i] == valor)) {
				selecionarModo(0, i, valor, VERTICAL);
				return true;
			}
		}
		if ((matriz[0][0] == matriz[1][1]) && (matriz[1][1] == matriz[2][2])
				&& (matriz[0][0] == valor)) {
			selecionarModo(0, 0, valor, DIAGONAL_L);
			return true;
		} else if ((matriz[0][2] == matriz[1][1])
				&& (matriz[1][1] == matriz[2][0]) && (matriz[0][2] == valor)) {
			selecionarModo(0, 2, valor, DIAGONAL_R);
			return true;
		}
		return false;
	}

	@Override
	public void notificar(int linha, int coluna, char valor)
			throws RemoteException {
		player1.vencer(linha, coluna, valor);
		player2.vencer(linha, coluna, valor);
	}

	@Override
	public void selecionarModo(int linha, int coluna, char valor, int tipo)
			throws RemoteException {
		switch (tipo) {
		case HORIZONTAL:
			notificar(linha, coluna, valor);
			notificar(linha, coluna + 1, valor);
			notificar(linha, coluna + 2, valor);
			break;
		case VERTICAL:
			notificar(linha, coluna, valor);
			notificar(linha + 1, coluna, valor);
			notificar(linha + 2, coluna, valor);
			break;
		case DIAGONAL_L:
			notificar(linha, coluna, valor);
			notificar(linha + 1, coluna + 1, valor);
			notificar(linha + 2, coluna + 2, valor);
			break;
		case DIAGONAL_R:
			notificar(linha, coluna, valor);
			notificar(linha + 1, coluna - 1, valor);
			notificar(linha + 2, coluna - 2, valor);
			break;
		}
	}
}