
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Classe servidor
 * 
 * Requisito: Java 8
 * 
 * @author douglas
 * @author ismaquias
 *
 */
public class Servidor {
	public static void main(String[] args) {
		try {
			
			String servidor = "10.42.0.63"; //ip do servidor
		
			System.setProperty("java.security.policy", "security.policy");
			System.setProperty("java.rmi.server.hostname",servidor);
						
			Jogo server = new Jogo();
			JogoInterface stub = (JogoInterface) UnicastRemoteObject.exportObject(server, 0);
			
			Registry registry =  LocateRegistry.createRegistry(1099);
	
            registry.bind("Jogo", stub);
            
			System.out.println("[Server] Aguardando jogadores...");

		} catch (Exception e) {
			System.out.println("[Server] Um erro ocorreu: " + e);

		}
	}
}