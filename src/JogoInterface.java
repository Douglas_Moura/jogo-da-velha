import java.rmi.Remote;
import java.rmi.RemoteException;


public interface JogoInterface extends Remote {
	
	public boolean logar(JogadorInterface jogador) throws RemoteException;
	public boolean jogar(int linha, int coluna, JogadorInterface jogador) throws RemoteException;
	public boolean vitoria(char jogo) throws RemoteException;
	public void notificar(int linha, int coluna, char valor) throws RemoteException;
	public void selecionarModo(int linha, int coluna, char valor, int tipo) throws RemoteException;
	public void iniciar() throws RemoteException;
	
	public JogadorInterface getPlayer1() throws RemoteException;
	public void setPlayer1(JogadorInterface player1) throws RemoteException;
	public JogadorInterface getPlayer2() throws RemoteException;
	public void setPlayer2(JogadorInterface player2) throws RemoteException;
	public JogadorInterface getRodada() throws RemoteException;
	public void setRodada(JogadorInterface rodada) throws RemoteException;
}
