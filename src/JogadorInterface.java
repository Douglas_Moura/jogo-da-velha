import java.rmi.Remote;
import java.rmi.RemoteException;


public interface JogadorInterface extends Remote {
	
	public char getNome() throws RemoteException;
	public void setNome(char nome) throws RemoteException;
	public void atualizarStatus(String status) throws RemoteException;
	public void atualizarGUI(int linha, int coluna, char valor) throws RemoteException;
	public void vencer(int linha, int coluna, char valor) throws RemoteException;
}
