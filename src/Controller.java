import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * Controller da classe cliente.
 * 
 * @author douglas
 * @author ismaquias
 */
public class Controller {
	@FXML
	private Label player;
	@FXML
	private TextField status;
	@FXML
	private Button um;
	@FXML
	private Button dois;
	@FXML
	private Button tres;
	@FXML
	private Button quatro;
	@FXML
	private Button cinco;
	@FXML
	private Button seis;
	@FXML
	private Button sete;
	@FXML
	private Button oito;
	@FXML
	private Button nove;

	private JogoInterface jogo;
	private JogadorInterface jogador;
	private Button matriz[][];

	/**
	 * Método responsável por realizar a conexao ao servidor.
	 * 
	 */
	private void conectar() {
		try {
			String cliente = "10.42.0.63"; // ip do cliente
			String servidor = "10.42.0.1"; // ip do servidor

			System.setProperty("java.security.policy", "security.policy");
			System.setProperty("java.rmi.server.hostname", cliente);

			Registry registry = LocateRegistry.getRegistry(servidor, 1099);

			jogo = (JogoInterface) registry.lookup("Jogo");
			jogador = new Jogador(this);
			jogo.logar(jogador);

		} catch (Exception e) {
			System.out.println("[Cliente] Falha ao conectar: " + e);
		}
	}

	@FXML
	private void initialize() {
		matriz = new Button[3][];
		matriz[0] = new Button[3];
		matriz[1] = new Button[3];
		matriz[2] = new Button[3];

		matriz[0][0] = um;
		matriz[0][1] = dois;
		matriz[0][2] = tres;
		matriz[1][0] = quatro;
		matriz[1][1] = cinco;
		matriz[1][2] = seis;
		matriz[2][0] = sete;
		matriz[2][1] = oito;
		matriz[2][2] = nove;

		conectar();
	}

	@FXML
	public void Um() {
		if (um.getText().equals("")) {
			try {
				jogo.jogar(0, 0, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	@FXML
	public void Dois() {
		if (dois.getText().equals("")) {
			try {
				jogo.jogar(0, 1, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	@FXML
	public void Tres() {
		if (tres.getText().equals("")) {
			try {
				jogo.jogar(0, 2, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	@FXML
	public void Quatro() {
		if (quatro.getText().equals("")) {
			try {
				jogo.jogar(1, 0, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	@FXML
	public void Cinco() {
		if (cinco.getText().equals("")) {
			try {
				jogo.jogar(1, 1, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	@FXML
	public void Seis() {
		if (seis.getText().equals("")) {
			try {
				jogo.jogar(1, 2, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	@FXML
	public void Sete() {
		if (sete.getText().equals("")) {
			try {
				jogo.jogar(2, 0, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	@FXML
	public void Oito() {
		if (oito.getText().equals("")) {
			try {
				jogo.jogar(2, 1, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	@FXML
	public void Nove() {
		if (nove.getText().equals("")) {
			try {
				jogo.jogar(2, 2, jogador);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println("[Cliente] Jogador O não conenctado...");
			}
		}
	}

	public void setPlayer(final char player2) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				player.setText(player.getText() + player2);
			}
		});
	}

	public void setStatus(final String status2) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				status.setText(status2);
			}
		});
	}

	public void setButton(final int linha, final int coluna, final char valor) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				matriz[linha][coluna].setText("" + valor);
			}
		});
	}

	public void setCor(final int linha, final int coluna, final char valor) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				matriz[linha][coluna].setStyle("-fx-text-fill: red;");
				;
			}
		});
	}
}